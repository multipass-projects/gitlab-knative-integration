# GitLab K3S integration

## Requirements

- you need `yq`
- 🖐️ I use `sed` and the syntax is different under OSX and Linux (I wrote my scripts for OSX)
  - OSX: `sed -i ''`
  - Linux: `sed -i` (remmove `''`)

> There is another way
>
> - install gnu coreutils with brew
> - `PATH="$(brew --prefix)/opt/coreutils/libexec/gnubin:$PATH"`
> - update the scripts with the Linux syntax

## Setup the integration on the K3S side

First, update the `integration.config` file with the path to the GitLab VM project and the K3S VM project

Then run (host side):

- `./01-update-hosts.sh` add the IP and the domain name to the hosts file of the K3S cluster
- `./02-fix-coredns.sh` update the **ConfigMap** of coredns
- `./03-admin-account.sh` create a `gitlab-admin` ServiceAccount
- `./04-get-certificate-token.sh` generate the certificate and the token in the `/secrets` path

## Setup the integration on the GitLAb side

go to your GitLab UI in the admin panel:

- add an existing cluster
- cluster name: `what-you-want`
- API URL: `https://192.168.64.27:6443` (check the ip of your cluster, you can find the appropriate url in this file: `k3s.yaml`)
- set the CA
- set the Token
- click on **"Install Helm Tiller"**
- click on **"Install Gitlab Runner"**

🎉 that's all

## One more thing

There is a bug on K3S (I need to double check), but you probably need to update coredns again when you restart your cluster. Then use `./fix-again-core-dns.sh`

#!/bin/sh
eval $(cat integration.config)
#echo ${cluster_vm_config_path}
#echo ${gitlab_vm_config_path}
eval $(cat ${cluster_vm_config_path}/vm.config)
cluster_vm_name=${vm_name}
#echo ${cluster_vm_name}
eval $(cat ${gitlab_vm_config_path}/vm.config)
gitlab_ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
gitlab_domain=${vm_domain}

echo "👋 Setup 🦊 integration"
echo "📝 apply some change to coredns"

# 👋 this command works only on OSX
# remove `''` if you are on Linux
cp coredns.patch.yaml.template ./coredns/coredns.patch.yaml
sed -i '' "s/GITLAB_IP/${gitlab_ip}/" ./coredns/coredns.patch.yaml
sed -i '' "s/GITLAB_DOMAIN/${gitlab_domain}/" ./coredns/coredns.patch.yaml

export KUBECONFIG=${cluster_vm_config_path}/config/k3s.yaml

kubectl get configmap coredns -n kube-system -o yaml > ./coredns/coredns.yaml
corefilepatch=$(yq read ./coredns/coredns.patch.yaml  'data.Corefile')
yq delete -i ./coredns/coredns.yaml 'data.Corefile'
yq write -i ./coredns/coredns.yaml 'data.Corefile' "${corefilepatch}"

kubectl apply -f ./coredns/coredns.yaml
kubectl delete pod --selector=k8s-app=kube-dns -n kube-system


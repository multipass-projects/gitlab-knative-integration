#!/bin/sh
eval $(cat integration.config)
#echo ${cluster_vm_config_path}
#echo ${gitlab_vm_config_path}
eval $(cat ${cluster_vm_config_path}/vm.config)
cluster_vm_name=${vm_name}
#echo ${cluster_vm_name}
eval $(cat ${gitlab_vm_config_path}/vm.config)
gitlab_ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
gitlab_domain=${vm_domain}

echo "👋 Setup 🦊 integration"
echo "📝 update of the /etc/hosts file of the cluster instance"
multipass --verbose exec ${cluster_vm_name} -- sudo -- bash <<EOF
echo "${gitlab_ip} ${gitlab_domain}" >> /etc/hosts
EOF


